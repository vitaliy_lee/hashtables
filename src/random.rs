extern crate rand;

use std::fmt;
use rand::{Rng};

pub struct HashSet {
	data: Vec<u64>,
    probes: usize,
    avg_probe: f64,
    max_probe: usize,
    fill: f64,
    size: usize,
}

impl HashSet {

    pub fn new(capacity: usize) -> HashSet {
        let data = vec![0; capacity];
        let set = HashSet{
            data,
            probes: 0,
            avg_probe: 0.0,
            max_probe: 0,
            fill: 0.0,
            size: 0
        };

        return set;
    }

}

impl super::Set for HashSet {

	fn put(&mut self, item: u64) {
        let length = self.data.len();
        let i = item as usize % length;
        let mut j = i;
        let mut probe = 0;

        let mut rnd = rand::ChaChaRng::new_unseeded();
        rnd.set_counter(j as u64, 1234567890u64);

        while self.data[j] != 0 {
            println!("Index {} is already taken", j);

            if probe == length {
                println!("Table is full");
                return
            }

            probe+=1;
            j = rnd.next_u32() as usize;
            j = j % length;

            println!("Probing next index: {:?}", j);
        }

        println!("Putting {:?} at index: {:?}", item, j);

        self.data[j] = item;
        self.size += 1;
        
        // Stats
        self.fill = self.size as f64 / self.data.len() as f64;
        self.probes += probe;

        if self.max_probe < probe {
            self.max_probe = probe
        }

        self.avg_probe = self.probes as f64 / self.size as f64;
	}

    fn find(&self, item: u64) -> Option<usize> {
        let length = self.data.len();
        let i = item as usize % length;
        let mut j = i;
        let mut probe = 0;

        let mut rnd = rand::ChaChaRng::new_unseeded();
        rnd.set_counter(j as u64, 1234567890u64);

        while self.data[j] != item {
            if probe == length {
                return None
            }

            probe+=1;
            j = rnd.next_u32() as usize;
            j = j % length;
        }

        return Some(i)
    }

    fn get_all(&self) -> Vec<u64> {
        self.data.clone().into_iter().
                filter(|item| *item != 0).collect()
    }

	fn to_json_string(&self) -> String {
        format!("{}", self)
	}
}


impl fmt::Display for HashSet {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Extract the value using tuple indexing
        // and create a reference to `vec`.
        let vec = &self.data;

        write!(f, "{{\"data\":[")?;

        // Iterate over `vec` in `v` while enumerating the iteration
        // count in `count`.
        for (count, v) in vec.iter().enumerate() {
            // For every element except the first, add a comma.
            // Use the ? operator, or try!, to return on errors.
            if count != 0 {
                write!(f, ", ")?;
            }

            write!(f, "{{\"value\":{}}}", v)?;
        }

        write!(f, "],")?;
        write!(f, "\"stats\":{{\"avg_probe\":{:.2},\"max_probe\":{},\"fill\":{:.2},\"size\":{}}}}}",
            self.avg_probe, self.max_probe, self.fill, self.size)
    }
}

#[test]
fn test_contains_all() {
    use super::*;

    const CAPACITY: usize = 20;
    const MIN: u64 = 100;
    const MAX: u64 = 999;

    let mut set = HashSet::new(CAPACITY);
    let mut rng = rand::ChaChaRng::new_unseeded();
    for _i in 0..CAPACITY {        
        let element: u64 = rng.gen_range(MIN, MAX);
        set.put(element);
    }

    let items = set.get_all();
    println!("Items {:?}", items);

    for item in items {
        let result = set.find(item);
        println!("Item {:?} found {:?}", item, result != None);
        assert!(result != None);
    }
}
