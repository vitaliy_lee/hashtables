extern crate qmlrs;
extern crate hashtables;

const DEFAULT_SIZE: usize = 5;

fn main() {
    let mut engine = qmlrs::Engine::new();

    let mut controller = hashtables::Controller::new();
    controller.fill(DEFAULT_SIZE);

    engine.set_property("controller", controller);
    engine.load_local_file("src/qml/main.qml");
    engine.exec();
}
