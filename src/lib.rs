#[macro_use]
extern crate qmlrs;

extern crate rand;
use rand::Rng;

const DEFAULT_CAPACITY: usize = 20;
const DEFAULT_MIN_VALUE: u64 = 100;
const DEFAULT_MAX_VALUE: u64 = 999;

mod linear;
mod quadratic;
mod random;

Q_OBJECT! {
    Controller:
        slot fn create(String, i64, i64, i64, i64);
        slot fn put();
        slot fn get_model();
        slot fn find_item(i64);
}

trait Set {
    fn put(&mut self, element: u64);
    fn find(&self, item: u64) -> Option<usize>;
    fn get_all(&self) -> Vec<u64>;
    fn to_json_string(&self) -> String;
}

pub struct Controller {
    set: Box<Set>,
    rng: rand::ChaChaRng,
    min: u64,
    max: u64
}

impl Controller {

    pub fn new() -> Controller {
        println!("Creating hash table");

        let set = Box::new(linear::HashSet::new(DEFAULT_CAPACITY));
        let rng = rand::ChaChaRng::new_unseeded();

        let controller = Controller{
            set,
            rng,
            min: DEFAULT_MIN_VALUE,
            max: DEFAULT_MAX_VALUE
        };

        return controller;
    }

    fn create(&mut self, probe_type: String, size: i64, capacity: i64, min: i64, max: i64) -> String {
        println!("Creating new hash table");

        self.min = min as u64;
        self.max = max as u64;
        self.rng = rand::ChaChaRng::new_unseeded();

        self.set = 
            match probe_type.as_str() {
                "linear" => Box::new(linear::HashSet::new(capacity as usize)),
                "quadratic" => Box::new(quadratic::HashSet::new(capacity as usize)),
                "random" => Box::new(random::HashSet::new(capacity as usize)),
                _ => panic!("Not supported probe type")
            };

        self.fill(size as usize);

        self.set.to_json_string()
    }

    pub fn fill(&mut self, size: usize) {
        println!("Filling hash table with values");

        for _i in 0..size {
            let element: u64 = self.rng.gen_range(self.min, self.max);
            self.set.put(element);
        }
    }

    fn put(&mut self) -> String {
        let element: u64 = self.rng.gen_range(self.min, self.max);
        println!("Putting element {:?}", element);
        self.set.put(element);

        self.set.to_json_string()
    }

    fn get_model(&self) -> String {
        self.set.to_json_string()
    }

    fn find_item(&self, item: i64) -> i64 {
        match self.set.find(item as u64) {
            Some(i) => i as i64,
            None    => -1
        }
    }

}
