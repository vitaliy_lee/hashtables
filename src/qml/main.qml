import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.2

ApplicationWindow {
    visible: true
    title: "Hash Tables Demo"

    width: 1280
    height: 480
    minimumWidth: 400
    minimumHeight: 300

    CreateTableDialog{
        id: createTableDialog
    }

    FindItemDialog{
        id: findItemDialog
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")

            MenuItem {
                shortcut: "Ctrl+N"
                text: qsTr("New")
                onTriggered: createTableDialog.open();
            }

            MenuItem {
                shortcut: "Ctrl+P"
                text: qsTr("Put")
                onTriggered: putItem();
            }

            MenuItem {
                shortcut: "Ctrl+F"
                text: qsTr("Find")
                onTriggered: findItemDialog.open();
            }

            MenuItem {
                shortcut: "Ctrl+Q"
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    Table {
        id: hashTable;
        width: parent.width; height: parent.height;
    }

    function putItem() {
        var data = controller.put();
        hashTable.reload(data);
    }

}
