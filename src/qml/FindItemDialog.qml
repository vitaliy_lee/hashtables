import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    title: "Find Item"
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    onAccepted: findItem();

    ColumnLayout {
            Label {
                text: "Item:"
            }

            TextField {
                id: itemField
                Layout.fillWidth: true
                focus: true
            }
    }

    function findItem() {
        var item = parseInt(itemField.text);
        var index = controller.find_item(item);
        if (index != -1) {
            console.log("Item index:", index);
            hashTable.focusOn(index);
        } else {
            console.log("Item not found");
            // hashTable.printMessage("Item not found");
        }
    }
}
