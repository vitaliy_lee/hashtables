import QtQuick 2.3

Rectangle {
    id: hashTable
    color: "black"

    property var highlightedItemIndex: -1
    property variant model : hashTableModel

    ListModel {
        id: hashTableModel
    }

    Grid {
        x: 5; y: 80
        rows: 2; spacing: 10

        Repeater { 
                  id: indexRepeater 
                  model: hashTableModel

                   Rectangle { width: 30; height: 30
                               color: "lightgreen"

                               Text { text: index
                                      font.pointSize: 16
                                      anchors.centerIn: parent } }
        }

        Repeater {
                  id: valueRepeater
                  model: hashTableModel

                  Rectangle { width: 30; height: 30
                               color: "violet"

                               Text { text: if (value == 0)
                                                ""
                                            else
                                                value

                                      font.pointSize: 16
                                      anchors.centerIn: parent } }

        }
    }

    Grid {
      x: 5; y: 380
      columns: 2

      Text {
        text: "Avg probe: "
        font.pointSize: 16
        color: "lightgreen"
      }

      Text {
        id: avgProbe
        font.pointSize: 16
        color: "lightgreen"
      }

      Text {
        text: "Max probe: "
        font.pointSize: 16
        color: "lightgreen"
      }

      Text {
        id: maxProbe
        font.pointSize: 16
        color: "lightgreen"
      }

      Text {
        text: "Fill: "
        font.pointSize: 16
        color: "lightgreen"
      }

      Text {
        id: fill
        font.pointSize: 16
        color: "lightgreen"
      }

      Text {
        text: "Size: "
        font.pointSize: 16
        color: "lightgreen"
      }

      Text {
        id: size
        font.pointSize: 16
        color: "lightgreen"
      }
    }

    Component.onCompleted: {
        var input = controller.get_model();
        reload(input)
    }

    function reload(input) {
        var model = JSON.parse(input);
        if (model === undefined) {
          console.log("Failed to parse JSON data");
        } else {
          avgProbe.text = model.stats.avg_probe
          maxProbe.text = model.stats.max_probe
          fill.text = model.stats.fill
          size.text = model.stats.size

          var data = model.data;
          hashTableModel.clear();
          for (var key in data) {
            var value = data[key];
            hashTableModel.append(value);
          }

          highlightedItemIndex = -1;
        }
    }

    function focusOn(index) {
        if (highlightedItemIndex != -1) {
          clearFocusAt(highlightedItemIndex);
        }

        var indexRect = indexRepeater.itemAt(index);
        indexRect.color = "lightyellow";
        var valueRect = valueRepeater.itemAt(index);
        valueRect.color = "lightyellow";
        highlightedItemIndex = index;
    }

    function clearFocusAt(index) {
        var indexRect = indexRepeater.itemAt(index);
        indexRect.color = "lightgreen";
        var valueRect = valueRepeater.itemAt(index);
        valueRect.color = "violet";
    }
}
