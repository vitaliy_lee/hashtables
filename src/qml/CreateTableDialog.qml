import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    title: "Create New Table"
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    onAccepted: createNewHashTable();

    ColumnLayout {
            GroupBox {
                title: "Probing Type"

                RowLayout {
                    ExclusiveGroup { id: probingTypeGroup }
                    RadioButton {
                        id: linearProbingTableType
                        text: "Linear"
                        checked: true
                        exclusiveGroup: probingTypeGroup
                    }
                    RadioButton {
                        id: quadraticProbingTableType
                        text: "Quadratic"
                        exclusiveGroup: probingTypeGroup
                    }
                    RadioButton {
                        id: randomProbingTableType
                        text: "Random"
                        exclusiveGroup: probingTypeGroup
                    }
                }
            }

            Label {
                text: "Size:"
            }

            TextField {
                id: sizeField
                text: "10"
                Layout.fillWidth: true
                focus: true
            }

            Label {
                text: "Capacity:"
            }

            TextField {
                id: capacityField
                text: "20"
                Layout.fillWidth: true
                focus: true
            }

            Label {
                text: "Min:"
            }

            TextField {
                id: minField
                text: "100"
                Layout.fillWidth: true
            }

            Label {
                text: "Max:"
            }

            TextField {
                id: maxField
                text: "999"
                Layout.fillWidth: true
            }
    }

    function createNewHashTable() {        
        var probingType
        if (linearProbingTableType.checked) {
            console.log("Creating new linear hash table");
            probingType = "linear";
        } else if (quadraticProbingTableType.checked) {
            console.log("Creating new quadratic hash table");
            probingType = "quadratic";
        } else {
            console.log("Creating new random hash table");
            probingType = "random";
        }

        var size = parseInt(sizeField.text);
        var capacity = parseInt(capacityField.text);
        var min = parseInt(minField.text);
        var max = parseInt(maxField.text);
        var model = controller.create(probingType, size, capacity, min, max);
        hashTable.reload(model);
    }
}
